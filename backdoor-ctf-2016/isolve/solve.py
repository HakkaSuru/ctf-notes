#!/usr/bin/python

from pwn import *
import re

words = []
f = open('dict.txt').read().splitlines()

conn = remote('hack.bckdr.in', 7070)
regex = ''

for i in range(0,48):
    print conn.recvline()
    print conn.recvuntil(':\n')
    regex = conn.recvline()[:-1]
    #print regex
    print conn.recvuntil(':\n')
    for word in f:
        if re.match(regex, word):
            #print word
            conn.sendline(word)
            break
        else:
            continue

print conn.recvall()
conn.close()
