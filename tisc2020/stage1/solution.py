import lzma, gzip, bz2, base64, binascii, zlib

f = open('temp.mess', 'rb')
d = f.read()
round = 0

while(True):
	round += 1
	print("round {0}".format(round))
	print("==================================")
	print(d)
	print("==================================")
	if (d[0:3] == b'\x1f\x8b\x08'):
		print("check gzip")
		d = gzip.decompress(d)
		continue
	elif (d[0:3] == b'BZh'):
		print("check bzip2")
		d = bz2.decompress(d)
		continue
	elif (d[0:6] == b'\xfd7zXZ\x00'):
		print("check lzma")
		d = lzma.decompress(d)
		continue
	elif (d[0:2] == b'x\x9c'):
		print("check zlib")
		d = zlib.decompress(d)
		continue
	else:
		print("check hexadecimal")
		try:
			d = binascii.unhexlify(d)
		except binascii.Error as err:
			print(err)
			print("check base64")
			d = base64.b64decode(d)
